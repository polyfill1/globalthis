
# Polyfill for globalThis
![license: MIT](https://badgen.net/npm/license/globalthis-polyfill)
[![gitlab](https://badgen.net/badge/icon/gitlab?icon=gitlab&label)](https://gitlab.com/polyfill1/globalthis/)
[![package health](https://snyk.io/test/npm/globalthis-polyfill/badge.svg)](https://snyk.io/advisor/npm-package/globalthis-polyfill)
 
## Introduction
Adds support for **globalThis** for old browsers and old NodeJS.
 
## Requirements
No requirements. This should work on any version of JavaScript.
 
## Installation
### Using NPM
[![install size](https://badgen.net/packagephobia/install/globalthis-polyfill)](https://packagephobia.com/result?p=globalthis-polyfill)
[![publish size](https://badgen.net/packagephobia/publish/globalthis-polyfill)](https://packagephobia.com/result?p=globalthis-polyfill)
``` console
$ npm install globalthis-polyfill
```
``` js
import 'globalthis-polyfill' // for ES2015 | ESNext
// or
require ('globalthis-polyfill') // for Node12 | CommonJS
```

### Using HTML script tag  
[![minified](https://badgen.net/bundlephobia/min/globalthis-polyfill)](https://bundlephobia.com/package/globalthis-polyfill)
[![minified + gzip](https://badgen.net/bundlephobia/minzip/globalthis-polyfill)](https://bundlephobia.com/package/globalthis-polyfill)
[![dependency count](https://badgen.net/bundlephobia/dependency-count/globalthis-polyfill)](https://bundlephobia.com/package/globalthis-polyfill)

``` html
<script src="https://cdn.jsdelivr.net/npm/globalthis-polyfill/index.min.js"></script>
```